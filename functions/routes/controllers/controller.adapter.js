function ControllerAdapter(Controller) {
    return async (req, res, next) => {
        const response = await Controller.handle(req)
        res.status(response.StatusCode).json(response.StatusMessage)
    }
}

module.exports = ControllerAdapter