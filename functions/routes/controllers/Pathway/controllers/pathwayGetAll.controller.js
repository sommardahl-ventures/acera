class PathwayGetAllController {
    pathwayService;
    constructor(pathwayService){
        this.pathwayService = pathwayService
    }

    async handle (req) {
        const response = await this.pathwayService.getAllPathwaysFromDatabase();
        return response
      }
}


module.exports = PathwayGetAllController