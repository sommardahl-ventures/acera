const BadRequest = require('../../../helpers/BadRequest')

class PathwaySaveController {
    pathwayService;
    constructor(pathwayService){
        this.pathwayService = pathwayService
    }

    async handle (req) {
        const pathwayBody = req.body
        let pathway;
        let pathwayFile;

        if (typeof req.files !== 'undefined') {
            if (typeof req.files.pathway !== 'undefined') {
                const rawdata = await req.files.pathway.data;        
                try {
                    pathwayFile = await JSON.parse(rawdata);
                } catch (error) {
                    return await BadRequest(error)
                }
            }
        }

        if (pathwayFile) {
            pathway = pathwayFile
        } else if (pathwayBody) {
            pathway = pathwayBody
        } else {
            return await BadRequest("none pathway was provided.")
        }
        const response = await this.pathwayService.savePathwayInDatabase(pathway);
        return response
      }
}

module.exports = PathwaySaveController

