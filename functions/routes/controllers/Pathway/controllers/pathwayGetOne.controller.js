class PathwayGetAllController {
    pathwayService;
    constructor(pathwayService){
        this.pathwayService = pathwayService
    }

    async handle (req) {
        const {id} = req.params
        const response = await this.pathwayService.getOnePathwayFromDatabase(id);
        return response
    }
}


module.exports = PathwayGetAllController