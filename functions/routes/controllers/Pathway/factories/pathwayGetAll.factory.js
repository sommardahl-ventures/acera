const PathwayGetAllController = require('./../controllers/pathwayGetAll.controller')
const PathwayService = require('../../../services/PathwayService')
const PathwayModel = require('../../../../models/pathways.models')

const DatabasePathway = require('../../../database/pathway')

function makePathwayGetAllController() {
    const pathwayModel = new PathwayModel()
    
    const pathwayService = new PathwayService(DatabasePathway, pathwayModel)
    
    const pathwayGetAllController = new PathwayGetAllController(pathwayService)
    
    return pathwayGetAllController
}

module.exports = makePathwayGetAllController