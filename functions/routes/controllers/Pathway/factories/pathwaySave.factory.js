const PathwaySaveController = require('../controllers/pathwaySave.controller')
const PathwayService = require('../../../services/PathwayService')
const PathwayModel = require('../../../../models/pathways.models')

const DatabasePathway = require('../../../database/pathway')

function makePathwaySaveController() {
    const pathwayModel = new PathwayModel()
    
    const pathwayService = new PathwayService(DatabasePathway, pathwayModel)
    
    const pathwaySaveController = new PathwaySaveController(pathwayService)
    
    return pathwaySaveController
}

module.exports = makePathwaySaveController