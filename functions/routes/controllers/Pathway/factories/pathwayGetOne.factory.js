const PathwayGetOneController = require('./../controllers/pathwayGetOne.controller')
const PathwayService = require('../../../services/PathwayService')
const PathwayModel = require('../../../../models/pathways.models')

const DatabasePathway = require('../../../database/pathway')

function makePathwayGetOneController() {
    const pathwayModel = new PathwayModel()
    
    const pathwayService = new PathwayService(DatabasePathway, pathwayModel)
    
    const pathwayGetOneController = new PathwayGetOneController(pathwayService)
    
    return pathwayGetOneController
}

module.exports = makePathwayGetOneController