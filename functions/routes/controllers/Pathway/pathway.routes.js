const express = require("express");
const router = express.Router();
const asyncMiddleware = require("../../middleware/AsyncMiddleware");
const authenticate = require("../../middleware/Authenticate");
const ControllerAdapter = require('../controller.adapter')

const makePathwaySaveController = require('./factories/pathwaySave.factory')
const makePathwayGetAllController = require('./factories/pathwayGetAll.factory')
const makePathwayGetOneController = require('./factories/pathwayGetOne.factory')

router.post(
  "/",
  asyncMiddleware(authenticate),
  asyncMiddleware(ControllerAdapter(makePathwaySaveController()))
);
router.get(
  "/",
  asyncMiddleware(authenticate),
  asyncMiddleware(ControllerAdapter(makePathwayGetAllController()))
);
router.get(
  "/:id",
  asyncMiddleware(authenticate),
  asyncMiddleware(ControllerAdapter(makePathwayGetOneController()))
);


module.exports = router;
