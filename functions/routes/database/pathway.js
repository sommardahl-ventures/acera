const database = require("../../FirebaseU/FirebaseAdmin").database;
const logger = require("firebase-functions").logger;
const pathwayFlattenHelper = require("../helpers/pathwayFlattenHelper");
const { modify } = require("../helpers/formatPathway");

async function GetAllPathwaysInDatabase() {
  let databaseQuery;
  let error;
  let res = [];
  try {
    await database
      .ref(`/pathways`)
      .once("value")
      .then((snapshot) => {
        databaseQuery = snapshot.val();
      });
    error = false;
    for (const index of Object.keys(databaseQuery)) {
      const match = databaseQuery[index];
      if (typeof match !== "undefined") {
        res.push(match);
      }
    }
  } catch (e) {
    logger.error(e, { structuredData: true });
    res = e;
    error = true;
  }
  return [error, res];
}
async function AddPathwayToDatabase(pathway) {
  let res;
  let error;
  let flattenResult = [];
  const pathwayID = Object.keys(pathway)[0];
  const pathwayValues = Object.values(pathway)[0];

  try {
    await database.ref(`/pathways/${pathwayID}`).set(pathwayValues);
    res = `Pathway ${pathwayValues.title} | ${pathwayID} it's now in DB`;
    error = false;
  } catch (e) {
    logger.error(e, { structuredData: true });
    res = e;
    error = true;
  }
  try {
    const [errorDatabase, allPathways] = await GetAllPathwaysInDatabase();
    const newPathway = modify(Object.values(pathway)[0], allPathways);
    const flattenPathway = pathwayFlattenHelper(newPathway, flattenResult);

    await database
      .ref(`/metadata/flattenPathways/${pathwayID}`)
      .set(flattenPathway);

    error = false;
    res = res + ` flattenPathway saved`;
  } catch (e) {
    logger.error(e, { structuredData: true });
    res = e;
    error = true;
  }
  return [error, res];
}

async function GetOnePathwayInDatabase(id) {
  let res;
  let error;
  try {
    await database
      .ref(`/pathways/${id}`)
      .once("value")
      .then((snapshot) => {
        res = snapshot.val();
      });
    error = false;
    if (res === null) {
      res = "ID does not exist.";
      error = true;
    }
  } catch (e) {
    logger.error(e, { structuredData: true });
    res = e;
    error = true;
  }
  return [error, res];
}

module.exports = {
  addPathwayToDatabase: AddPathwayToDatabase,
  getAllPathwaysInDatabase: GetAllPathwaysInDatabase,
  getOnePathwayInDatabase: GetOnePathwayInDatabase,
};
