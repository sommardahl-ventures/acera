/*const { addPathwayToDatabase } = require("./pathway");
const pathway = require("./../../pathways/pG9FIu8VR32QEjVHvb1Yog.json");
const pathway0 = require("./../../pathways/pG9FIu8VR32QEjVHvb1Yog5.json");
const pathway1 = require("./../../pathways/0KgV-_M9Ske1S7wOV4Ejvg5.json");
const pathway2 = require("./../../pathways/zmI_1cMZQvKnHDQpvgBVzQ.json");
const pathway3 = require("./../../pathways/0KgV-_M9Ske1S7wOV4Ejvg6.json");
const pathway4 = require("./../../pathways/1fzG9GlBT8-pwIV3BT9TZg.json");
const pathway5 = require("./../../pathways/7_5DP_HSRRO8HWBa-b7D5Q.json");
const pathway6 = require("./../../pathways/FZvBK3R8RZS6_rhJZd7asA.json");
const pathway7 = require("./../../pathways/I4w9NAXTTLqD4YQO2WCIEw.json");
const pathway8 = require("./../../pathways/J-xxphepR6C-A1mCuB4Tzg.json");
const pathway9 = require("./../../pathways/LyTaOWWTSveGIts6UfAPzA.json");
const pathway10 = require("./../../pathways/NYQRI1lSS8e0bej0uh5Yrw.json");
const pathway11 = require("./../../pathways/P6d_xH8jR6eqHznHx8o8bg.json");
const pathway12 = require("./../../pathways/RTT5x_LARPSEtN4z82Q6Aw.json");
const pathway13 = require("./../../pathways/vv55LzapSK6YU9ZyjC5OUw.json");
const pathway14 = require("./../../pathways/xudFBxqrR7eZlL_-28nmxQ.json");
const pathway17 = require("./../../pathways/zok5-jfYQ4uYm89_4b632g.json");
const pathway18 = require("./../../pathways/Wne9mvOTQf6i6K930H1JZA.json");
const pathway19 = require("./../../pathways/KzkrC1E0SZyQtd4JRfcdLQ.json");
const pathway20 = require("./../../pathways/e0N7JVktSDWONWtba_w2rw.json");
const pathway21 = require("./../../pathways/akdw2yaJRiK_BwIjGlWHbA.json");
const pathway22 = require("./../../pathways/gWSTOz89SjSEVu5nEu-t0Q.json");
const pathway23 = require("./../../pathways/u-W0T22oQr-FGGwQXr0BUg.json");
const pathway24 = require("./../../pathways/0HMMjjtPRGitplfVTB1XMQ.json");
const pathway25 = require("./../../pathways/WyXjBnsQSeexklhQclWFmQ.json");
const pathway26 = require("./../../pathways/Ailbk_v2Q1aBoMxhhHRxBg.json");
const pathway27 = require("./../../pathways/2l_3VFY5QtSrkC_U_dYH9w.json");
const pathway28 = require("./../../pathways/8rfyWvDgQcKvWlNFgJm1Sw.json");
const pathway29 = require("./../../pathways/ufmR4IouRsKpDEmt9y2k0w.json");
const pathway30 = require("./../../pathways/Yaavzc6JTxa_C71gPG29fQ.json");
const pathway31 = require("./../../pathways/2icBqJ_RRDC4aaAko8PzVA.json");
const pathway32 = require("./../../pathways/54UbPa8aQi2DvHdIOylc_w.json");
const pathway33 = require("./../../pathways/U7bDGHhNSySaXNzetvhD9g.json");
const pathway34 = require("./../../pathways/jNwViywlRDepc-gXcrv5Kg.json");

jest.setTimeout(50000000);

describe("database pathway", () => {
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway0);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway1);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway2);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway3);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway4);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway5);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway6);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway7);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway8);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway9);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway10);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway11);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway12);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway13);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway14);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway17);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway18);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway19);

    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway20);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway21);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway22);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway23);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway24);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway25);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway26);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway27);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway28);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway29);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway30);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway31);
    expect(err).toBe(false);
  });

  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway32);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway33);
    expect(err).toBe(false);
  });
  test("should save pathway in database and save flatten pathway in db", async () => {
    const [err, res] = await addPathwayToDatabase(pathway34);
    expect(err).toBe(false);
  });
});
*/