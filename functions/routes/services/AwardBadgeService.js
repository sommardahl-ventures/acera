const axios = require("axios");
const envs = require("../../env.json");
const ISSUER_ID = envs.service.issuer_id;
const APP_URL = envs.service.app_url;
const fetch = require("node-fetch");
const logger = require("firebase-functions").logger;
const AWARDS = {};
const ONE_HOUR = 60 * 60 * 1000;
const FirebaseAdmin = require("../../FirebaseU/FirebaseAdmin");
const WebhookService = require("./WebhookService");
var NO_UPDATE = true;

const requireUncached = (module) => {
  delete require.cache[require.resolve(module)];
  return require(module);
};

const badgeAward = async (data, authToken) => {
  let responseOut;
  await fetch(
    `https://api.badgr.io/v2/badgeclasses/${data.badgeToken}/assertions`,
    {
      method: "POST",
      body: JSON.stringify({
        recipient: {
          identity: data.email,
          type: "email",
          hashed: true,
        },
        notify: true,
      }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${authToken}`,
      },
    }
  )
    .then((res) => {
      return res.json();
    })
    .then(async (response) => {
      responseOut = response;
      if(!responseOut.status.success) {
        logger.error("Error in Awarding process", responseOut, { structuredData: true });
      }else{
        logger.info("responseOut", responseOut, { structuredData: true });
        await WebhookService.runAwardWebhooks(response);
      }

    })
    .catch((error) => console.error("AwardBadgeService ", error));

  return responseOut;
};

const AwardService = {
  awardBadge: async (data, authToken) => {
    let response = await badgeAward(data, authToken);
    return response;
  },
  listAwards: async (data, authToken) => {
    var d = new Date();
    var time = d.getTime();

    if (AWARDS["list"] && time - AWARDS["time"] < ONE_HOUR && NO_UPDATE) {
      return AWARDS["list"];
    }
    let response = await axios({
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
      method: "get",
      url: `/issuers/${ISSUER_ID}/assertions`,
    });

    if (response.data) {
      AWARDS["list"] = response.data;
      AWARDS["time"] = time;
    }

    return response.data;
  },
};

module.exports = AwardService;
