const Ok = require('../helpers/Ok')
const BadRequest = require('../helpers/BadRequest');
const OkSendData = require('../helpers/OkSendData');

class PathwayService  {
    DatabasePathway;
    PathwayModel;
    constructor(DatabasePathway, PathwayModel){
     this.DatabasePathway = DatabasePathway
     this.PathwayModel = PathwayModel
    }
    async savePathwayInDatabase  (pathway) {
        const [error, pathMsg] = this.PathwayModel.validate(pathway);
        if(error === false){
            return pathMsg
        }
        const [dbError, dbMessage] = await this.DatabasePathway.addPathwayToDatabase(pathMsg);
        if(dbError === true){
            return BadRequest(dbMessage)
        }
        return Ok(dbMessage)
    }
    async getAllPathwaysFromDatabase () {
        const [dbError, dbMessage] = await this.DatabasePathway.getAllPathwaysInDatabase();
        if(dbError === true){
            return BadRequest(dbMessage)
        }
        return OkSendData(dbMessage)
    }
    async getOnePathwayFromDatabase (id) {
        const [dbError, dbMessage] = await this.DatabasePathway.getOnePathwayInDatabase(id);
        if(dbError === true){
            return await BadRequest(dbMessage)
        }
        return OkSendData(dbMessage)
    }    
};

  module.exports = PathwayService;
  