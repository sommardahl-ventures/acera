function MissingParamError(field) {
    return {
        StatusCode: 400,
        StatusMessage: {
            error: `${field} is required`
        }
    }
}

module.exports = MissingParamError