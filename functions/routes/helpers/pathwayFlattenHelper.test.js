const pathwayMapTransform = require("./pathwayFlattenHelper");
const databasePathway = require("./../database/pathway");
const util = require("util");
const { modify } = require("./formatPathway");
/*const {
  zmI_1cMZQvKnHDQpvgBVzQ,
} = require("../../pathways/zmI_1cMZQvKnHDQpvgBVzQ.data");
const {
  NYQRI1lSS8e0bej0uh5Yrw,
} = require("../../pathways/NYQRI1lSS8e0bej0uh5Yrw.data");
const pathway1 = require("../../pathways/0KgV-_M9Ske1S7wOV4Ejvg.json");
const pathway2 = require("../../pathways/I4w9NAXTTLqD4YQO2WCIEw.json");
const pathway3 = require("../../pathways/NYQRI1lSS8e0bej0uh5Yrw.json");
const fullstackPathway = require("../../pathways/zmI_1cMZQvKnHDQpvgBVzQ.json");*/
const test0 = [
  {
    "title": "Acera-Test-Card122-Scenario3",
    "children": [
      {
        "title": "Acera Test 103",
        "completionBadge": "https://badgr.com/public/badges/kdMQ8UUnT5-Qd7nBsZR8dg",
        "requiredBadge": "https://badgr.com/public/badges/tIqnWZKzSoa4J2z_XS6zzg",
        "children": [
          {
            "title": "Acera Test 105",
            "requiredBadge": "https://badgr.com/public/badges/MTqvYOTaT1SL0F6i54KxmA"
          }
        ]
      }
    ],
    "completionBadge": "https://badgr.com/public/badges/tVHdLV6jQFefq4nZ_cbfTA"
  }
]
const test1 = [
  {
    "title": "Acera-Test-Card122-Scenario4",
    "children": [
      {
        "title": "Acera Test 108",
        "requiredBadge": "https://badgr.com/public/badges/8urFAPX-QiOUowi856STxw"
      }
    ],
    "completionBadge": "https://badgr.com/public/badges/CADOJMpiS5qUUbr9q9456w",
    "requiredBadge": "https://badgr.com/public/badges/CJpy_Qx-TxuD6LNIY27rpQ"
  }
];

const test2 = [
  {
    "title": "Acera-Test-Card122-Scenario4",
    "children": [
      {
        "title": "Acera Test 108",
        "requiredBadge": "https://badgr.com/public/badges/8urFAPX-QiOUowi856STxw"
      },
      {
        "title": "Acera Test 109",
        "requiredBadge": "https://badgr.com/public/badges/testing"
      },
      {
        "title": "x",
        "completionBadge": "https://badgr.com/public/badges/completion",
        "children": [
          {
            "title": "x",
            "requiredBadge": "https://badgr.com/public/badges/required"
          }
        ]
      }
    ],
    "completionBadge": "https://badgr.com/public/badges/CADOJMpiS5qUUbr9q9456w",
    "requiredNumber": "1"
  }
];

const fullstackPathway = [
  {
    "title": "Full-Stack Developer 1",
    "children": [
      {
        "pathwayURL": "pathway/bXQp-ii-SP-mdOoY9hRWvQ",
        "title": "Personal Grower",
        "completionBadge": "https://badgr.com/public/badges/bXQp-ii-SP-mdOoY9hRWvQ"
      },
      {
        "pathwayURL": "pathway/j4hIuU1mSvyk8Q1Xi0dhmw",
        "title": "Professional Skills",
        "completionBadge": "https://badgr.com/public/badges/j4hIuU1mSvyk8Q1Xi0dhmw"
      },
      {
        "title": "Technical Skills",
        "children":[
          {
            "title": "Design Patterns",
            "children": [
              {
                "title": "Adapter Pattern",
                "requiredBadge": "https://badgr.com/public/badges/-AXf3JrPSsKZ50-qiMj2xA"
              },
              {
                "title": "Strategy Pattern",
                "requiredBadge": "https://badgr.com/public/badges/X9R3b7psSiGDA4OowV9Wpw"
              }
            ]
          },
          {
            "title": "Tools",
            "children": [
              {
                "title": "Git",
                "requiredBadge": "https://badgr.com/public/badges/upn2ozyZRNmui7F4SJmsuQ"
              },
              {
                "title": "Code Editor",
                "requiredBadge": "https://badgr.com/public/badges/0F_Lcp-3TK-e8gdMcpffEw"
              },
              {
                "title": "Terminal",
                "requiredBadge": "https://badgr.com/public/badges/Ug4EHYPuTtuLEGwrH85uGQ"
              },
              {
                "title": "Trello",
                "requiredBadge": "https://badgr.com/public/badges/ebAZzcYNQ7iTq2Bz8C7pQA"
              }
            ]
          },
          {
            "title": "Applications",
            "requiredBadge": "https://badgr.com/public/badges/fygDwVQkQe--skqXNqsPeQ",
            "children": [
              {
                "title": "SPA Front-End",
                "requiredBadge": "https://badgr.com/public/badges/mpP6EehkQKWaLMNdlHof7A"
              },
              {
                "title": "Rest API Back-End ",
                "requiredBadge": "https://badgr.com/public/badges/2d_P5eloSwSxjxyhYEoF-A"
              }
            ]
          },
          {
            "title": "Languages",
            "children": [
              {
                "title": "Javascript",
                "requiredBadge": "https://badgr.com/public/badges/vKmoeMs2Q-2nFVhYpCufUA"
              },
              {
                "title": "C#",
                "requiredBadge": "https://badgr.com/public/badges/Qup2615jQoi0WAdyO97zPw"
              },
              {
                "title": "SQL",
                "requiredBadge": "https://badgr.com/public/badges/61D_nPfvTl-tXmZrg5EtKQ"
              }
            ]
          }
        ]
      }
    ],
    "completionBadge": "https://badgr.com/public/badges/zmI_1cMZQvKnHDQpvgBVzQ"
  }
];

const onboarding = [
  {
    "title" : "Boarding Pass",
    "completionBadge": "https://api.badgr.io/public/badges/vv55LzapSK6YU9ZyjC5OUw",
    "children": [{
        "title": "Who We Are",
        "children":[{
            "title": "Meet the Team",
            "requiredBadge": "https://api.badgr.io/public/badges/VCQM9csRTNGKKkQU_DaSSw",
            "children": [{
                "title": "Building Community",
                "requiredBadge": "https://api.badgr.io/public/badges/G5QMrpYdRRCr-Yv8_SMu6A",
                "children": [{
                    "title": "Welcome to Our Family",
                    "requiredBadge": "https://api.badgr.io/public/badges/KvfjtW2QQH63DtgWpwULbg"
                }]
            }]
        },{
            "title": "Culture Champion",
            "requiredBadge": "https://api.badgr.io/public/badges/jhsREQiaRWqOM7J0OeL8aA"
        },{
            "title": "Our Vision and Values",
            "requiredBadge": "https://api.badgr.io/public/badges/hKZ2PgMXQEyVVegcw0aVNg"
        }]
    },{
        "title": "Meeting with Guild Master",
        "requiredBadge": "https://api.badgr.io/public/badges/_pO-ETR0SVyB-y7GMh5mCA",
        "children": [{
            "title": "Operations",
            "children": [{
                "title": "Introduction to Client Expectations",
                "requiredBadge": "https://api.badgr.io/public/badges/GPM-Orr-QHCV_9S0aX-_SQ"
            },{
                "title": "Remote Professional",
                "requiredBadge": "https://api.badgr.io/public/badges/tOF9eApiR664olfo5lfeVg"
            },{
                "title": "Tool Box",
                "requiredBadge": "https://api.badgr.io/public/badges/eF_cwwamSLyP0WkeTRDRrA"
            },{
                "title": "Atmosphere of Creativity",
                "requiredBadge": "https://api.badgr.io/public/badges/wIizrBoYQ16gJZxq2y9Aqw"
            }]
        }]
    }]
  }
];

describe("make pathway flatten", () => {
  test("MOdify the fullstackPathway and flatten", async () => {
    const [error, pathways] = await databasePathway.getAllPathwaysInDatabase();
    let result = [];
    const newPathway = modify(Object.values(fullstackPathway)[0], pathways);
    const response = pathwayMapTransform(newPathway, result);
    console.log(response)

    expect(error).toBe(false);
  });

});
