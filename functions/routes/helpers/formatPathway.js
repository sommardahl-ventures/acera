const getID = (str) => str.substring(str.lastIndexOf("/") + 1);
const { inspect } = require('util');
function modify(pathway, pathways) {
  const newChildren = [];
  if (pathway) {
    if (pathway.children) {
      for (let index = 0; index < pathway.children.length; index++) {
        const newChild = modifyaux(pathway.children[index], pathways);
        if (newChild) newChildren.push(newChild);
      }
    }
  }
  let newPathway = {};
  if(pathway.requiredNumber){
    if(pathway.requiredBadge){
      newPathway = {
        [getID(pathway.completionBadge)]: {
          children: newChildren,
          title: pathway.title,
          completionBadge: pathway.completionBadge,
          requiredBadge: pathway.requiredBadge,
          requiredNumber: pathway.requiredNumber
        },
      };
    }else{
      newPathway = {
        [getID(pathway.completionBadge)]: {
          children: newChildren,
          title: pathway.title,
          completionBadge: pathway.completionBadge,
          requiredNumber: pathway.requiredNumber
        },
      };
    }
  }else{
    if(pathway.requiredBadge){
      newPathway = {
        [getID(pathway.completionBadge)]: {
          children: newChildren,
          title: pathway.title,
          completionBadge: pathway.completionBadge,
          requiredBadge: pathway.requiredBadge
        },
      };
    }else{
      newPathway = {
        [getID(pathway.completionBadge)]: {
          children: newChildren,
          title: pathway.title,
          completionBadge: pathway.completionBadge
        },
      };
    }
  }
  return newPathway;
}

function modifyaux(pathway, pathways) {
  let newPathway = pathway;
  if (pathway) {
    const newChildren = [];
    let oldChildren = [];
    if (pathway.pathwayURL) {
      let childPathway = pathways.filter(
        (path) => getID(path.completionBadge) === getID(pathway.pathwayURL)
      );
      
  if (childPathway.length > 0 && childPathway[0].children) {

        newPathway.title = pathway.title ? pathway.title.split("-").join(" ")
        :  childPathway[0].title.split("-").join(" ");

        if(!pathway.requiredBadge) {
          newPathway.completionBadge = pathway.completionBadge || childPathway[0].completionBadge;
        }
        if(!pathway.completionBadge){
          newPathway.requiredBadge = pathway.requiredBadge || childPathway[0].requiredBadge;
        }
          
        if (!newPathway.children) {
          newPathway.children = childPathway[0].children;
        } else {
          oldChildren = newPathway.children;
          newPathway.children = childPathway[0].children;
          newPathway = addChildrenAtDeep(oldChildren, newPathway);
        }
      }
    }

    if (newPathway.children) {
      for (let index = 0; index < newPathway.children.length; index++) {
        const newChild = modifyaux(newPathway.children[index], pathways);
        if (newChild) newChildren.push(newChild);
      }
      newPathway.children = newChildren;
    }
  }
  return newPathway;
}

function addChildrenAtDeep(oldChildren, pathway) {
  const newPathway = pathway;

  if (newPathway) {
    if (newPathway.children && newPathway.children.length > 0) {
      for (let index = 0; index < newPathway.children.length; index++) {
        addChildrenAtDeep_aux(oldChildren, newPathway.children[index]);
      }
    } else {
      newPathway.children = oldChildren;
    }
  }

  return newPathway;
}

function addChildrenAtDeep_aux(oldChildren, pathway) {
  const newPathway = pathway;

  if (newPathway) {
    if (newPathway.children) {
      const newChildren = [];
      for (let index = 0; index < newPathway.children.length; index++) {
        const child = addChildrenAtDeep_aux(
          oldChildren,
          newPathway.children[index]
        );
        if (child) newChildren.push(child);
      }
      newPathway.children = newChildren;
    } else {
      newPathway.children = oldChildren;
    }
  }
  return newPathway;
}

module.exports = {
  modify,
};
