function OkSendData(data) {
    return {
        StatusCode: 200,
        StatusMessage: data
    }
}

module.exports = OkSendData