async function BadRequest(message) {
    return {
        StatusCode: 400,
        StatusMessage: { 
            error: `${message}` 
        }
    }
}

module.exports = BadRequest