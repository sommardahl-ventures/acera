const functions = require('firebase-functions');
const express = require('express');
const path = require('path');
const axios = require('axios');
const bodyParser = require('body-parser');
const envs = require('./env.json');
const cors = require('cors');
const fireAdmin = require('./FirebaseU/FirebaseAdmin');
const qs = require('querystring');
const rollbar = require('./rollbar.js')
const awardCronFunction = require('./CronJobs/awardCronJob')
const refreshToken = require('./CronJobs/refreshTokenCronJob')
const fileupload = require('express-fileupload')

const PRIVATE_KEY = envs.service.private_key;
const DOMAIN = envs.service.domain;
const APP_URL = envs.service.app_url;
const EMAIL = envs.service.badge_owner_email;
const AWARDING_TIME = envs.service.awarding_time;
const REFRESHING_TIME = envs.service.refreshing_time;
const ROLLBAR_USE = envs.service.rollbar_use;

const app = express();
//Add as a middleware
const corsOptions = {
  origin: true,
  methods:['GET','POST','DELETE','UPDATE','OPTIONS'],
};

app.use(cors(corsOptions))
console.log("Rollbar use: ", ROLLBAR_USE)
if(ROLLBAR_USE){
  app.use(rollbar.rollbar.errorHandler());
  console.log("THIS IS A TEST")
  rollbar.rollbar.error("testing dev")
}


axios.defaults.baseURL = envs.service.base_url;

const badgeController = require('./routes/controllers/BadgeController');
const ClaimBadgeController = require('./routes/controllers/ClaimBadgeController');
const awardBadgeController = require('./routes/controllers/AwardBadgeController');
const issuerController = require('./routes/controllers/IssuerController');
const userController = require('./routes/controllers/UserAuthController');
const pathwayController = require('./routes/controllers/Pathway/pathway.routes');

const userService = require('./routes/services/UserAuthService');
const authenticate = require('./routes/middleware/Authenticate');
const awardservice = require('./routes/services/AwardBadgeService');

const { fn } = require('jquery');
const { userInfo } = require('os');

const mailgun = require('mailgun-js')({apiKey: PRIVATE_KEY, domain: DOMAIN});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(fileupload())

app.use('/api/badges', badgeController);

app.use('/api/claim', ClaimBadgeController);

app.use('/api/issuer', issuerController);

app.use('/api/award', awardBadgeController);

app.use('/api/users', userController);

app.use('/api/pathways', pathwayController);

app.post('/api/pathways/:pathwayId/subscribe' , (req, res) => {
  var data = {
    from: req.body.from,
    to: req.body.to,
    subject: `Subscription to ${req.body.pathway} pathway`,
    html: `Hello, 
          <br><br>
          
          We have a request from ${req.body.from} to subscribe to the pathway:
          <p>Name: ${req.body.pathway}</p>
          <p>ID: ${req.params.pathwayId}</p>
          <br><br>

          Thanks,
          <br><br>

          Badgr Extras Extension`
  };

  mailgun.messages().send(data, function (error, body) {
    res.status(200).send('OK');
  });
});

app.post('/api/v2/pathways/:pathwayId/subscribe', (req, res) => {
  var data = {
    from: req.body.from,
    to: req.body.to,
    subject: `Subscription to ${req.body.pathway} pathway`,
    html: `Hello, 
          <br><br>
          
          We have a request from ${req.body.from} to subscribe to the pathway:
          <p>Name: ${req.body.pathway}</p>
          <p>ID: ${req.params.pathwayId}</p>
          <br><br>

          Thanks,
          <br><br>

          Badgr Extras Extension`
  };

  mailgun.messages().send(data, function (error, body) {
    res.status(200).send('OK');
  });
});

app.post('/api/invite', (req, res) => {
  var data = {
    from: EMAIL,
    to: req.body.payload.email,
    subject: `Invitation to ${req.body.payload.name} group`,
    html: `Hello,
          <br><br>
          You were added to the 
          <b>${req.body.payload.name}</b> group.
          <br><br>
          Access your pathways at: ${APP_URL}pathway
          <br><br>
          Regards,
          <br><br>
          Badgeways`
  };

  mailgun.messages().send(data, function (error, body) {
    res.status(200).send('OK');
  });
});

app.post('/api/newpathway', (req, res) => {
  var data = {
    from: EMAIL,
    to: req.body.payload.email,
    subject: `New pathway available in your ${req.body.payload.groupname} group`,
    html: `Hello,
          <br><br>
          A new pathway called <b>${req.body.payload.pathname}</b> has been added to your 
          <b>${req.body.payload.groupname}</b> group.
          <br><br>
          Check your new pathway at: ${APP_URL}pathway/${req.body.payload.pathwayid}
          <br><br>
          Regards,
          <br><br>
          Badgeways`
  };

  mailgun.messages().send(data, function (error, body) {
    res.status(200).send('OK');
  });
});

app.post('/api/users/getToken', async (req, response) => {
  var res;
  try {
    res = await axios({
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      method: 'post',
      url: 'https://api.badgr.io/o/token' ,
      data: req.body.data,
    });
  } catch (error) {
    console.log("An error ocurred", error);
    if(rollbar.isActivated){
      rollbar.rollbar.error("Users getToken Fails", error)
    }
  }

  var res1 = [];

  if (res.status == 200) {
    res1 = await axios({
      headers: {
        Authorization: `Bearer ${res.data.access_token}`,
      },
      method: 'get',
      url: `https://api.badgr.io/v2/issuers`,
    });
    response.status(200).send(JSON.stringify(res1.data));
  } else {
    response.status(400).send("NO ISSUERS");
  }
});

app.post('/api/users/hi', async(req, res) => {
  console.log("HERE!")
  rollbar.rollbar.log("Hello world!");
  res.send(`This is the badge claim api from staging V3`);
});

functions.database.ref(`/groups/`).onUpdate((snap, context) => {
  console.log(snap.val(), "written by", context.auth.uid);
});

// FIREBASE FUNCTIONS PER ENVIRONMENT (Dev, Stag, Prod) //

exports.awardCronJob = functions.pubsub
  .schedule(`every ${AWARDING_TIME} minutes`)
  .onRun(async (context) => {
    awardCronFunction.awardCronFunction(context)
  });

exports.refreshTokenJob = functions.pubsub
  .schedule(`every ${REFRESHING_TIME} minutes`)
  .onRun(async (context) => {
    refreshToken.refreshToken(context);
});

exports.awardCronJobdev = functions.pubsub
  .schedule(`every ${AWARDING_TIME} minutes`)
  .onRun(async (context) => {
    awardCronFunction.awardCronFunction(context)
  });

exports.refreshTokenJobdev = functions.pubsub
  .schedule(`every ${REFRESHING_TIME} minutes`)
  .onRun(async (context) => {
    refreshToken.refreshToken(context);
});

exports.awardCronJob_staging = functions.pubsub
  .schedule(`every ${AWARDING_TIME} minutes`)
  .onRun(async (context) => {
    awardCronFunction.awardCronFunction(context)
  });


exports.refreshTokenJob_staging = functions.pubsub
  .schedule(`every ${REFRESHING_TIME} minutes`)
  .onRun(async (context) => {
    refreshToken.refreshToken(context);
});

exports.testWebHook = functions.https.onRequest(async (req, res) => {
  functions.logger.info("TestWebHook", req.body, { structuredData: true })
  res.json(req.body);
});

exports.app = functions.https.onRequest(app);
exports.appdev = functions.https.onRequest(app);
exports.app_staging = functions.https.onRequest(app);

