const { awardCronFunction } = require("./awardCronJob");

jest.setTimeout(50000000);

describe("Award Cron Job function", () => {
  test("should run the cronJob", async () => {
    const response = await awardCronFunction();
    expect(response).toBe(null);
  });
});
