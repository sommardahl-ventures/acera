const database = require("../../../FirebaseU/FirebaseAdmin").database;

class FlattenPathways {
  constructor() {}
  async getAllFlattenPathways() {
    return await database
      .ref("/metadata/flattenPathways")
      .once("value")
      .then((snapshot) => snapshot.val());
  }
}

module.exports = FlattenPathways;
