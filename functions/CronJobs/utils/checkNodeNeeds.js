const getBadgeId = require('./getBadgeId')

const checkNodeNeeds = (node, badges) => {
  let result = 0;
  for (const badge of badges) {
    if (node.needs.includes(getBadgeId(badge.id))) {
      result++;
    }
  }
  if(node.requiredNumber && node.requiredNumber !== "0"){
    return result >= parseInt(node.requiredNumber);
  }else{
    return result === node.needs.length;
  }
};

module.exports = checkNodeNeeds
