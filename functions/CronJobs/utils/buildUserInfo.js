const buildUserInfo = (email, access_token, badges, pathways, groups) => {
  let pathwaysFiltered = [];

  pathways.forEach((pathway) => {
    if (!pathwaysFiltered.includes(pathway)) {
      pathwaysFiltered.push(pathway);
    }
  });

  return {
    user: {
      email,
    },
    backpack: {
      access_token,
    },
    badges,
    pathways: pathwaysFiltered,
    groups,
  };
};

module.exports = buildUserInfo
