const isTokenExpired = (issuedDate, expires_in) => {
  const today = new Date();
  const time = today.getTime();

  if (time - issuedDate < expires_in) {
    console.log("Token expired");
    return true;
  }
  return false;
};

module.exports = isTokenExpired
