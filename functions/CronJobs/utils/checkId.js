const getBadgeId = require('./getBadgeId')

const checkId = (id, badges) => {
  let result = false;
  for (const badge of badges) {
    if (getBadgeId(badge.id) === id) {
      result = true;
    }
  }
  return result;
};

module.exports = checkId
