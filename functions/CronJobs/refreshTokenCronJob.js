const fireAdmin = require("../FirebaseU/FirebaseAdmin");
const envs = require("../env.json");
const SECRET = envs.service.secret;
const CLIENT_ID = envs.service.client_id;
const qs = require("querystring");
const { isTokenExpired } = require("./utils");

const refreshToken = async (context) => {
  console.log("Inside of Refresh Token");
  const snapshotUsers = await fireAdmin.getUsers().once("value");
  const users = Object.values(snapshotUsers.val());

  for (let i = 0; i < users.length; i++) {
    if (users[i].backpacks) {
      let expired = isTokenExpired(
        Object.values(users[i].backpacks)[0].issuedOn,
        Object.values(users[i].backpacks)[0].data.expires_in
      );
      if (expired) {
        let response;
        var dataRef = qs.stringify({
          refresh_token: Object.values(users[i].backpacks)[0].data
            .refresh_token,
          grant_type: "refresh_token",
          client_id: CLIENT_ID,
          client_secret: SECRET,
        });
        var config = {
          method: "post",
          url: "https://api.badgr.io/o/token",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          data: dataRef,
        };

        await axios(config)
          .then(function (res) {
            response = res.data;
          })
          .catch(function (error) {
            console.log("Error: ", error);
          });

        if (response === "") {
          console.log("Empty Data: ", users[i].profile.email);
        } else {
          fireAdmin.saveBackpackToken(
            users[i].profile.email,
            response,
            users[i].profile.email
          );
        }
      }
    }
  }
};

module.exports = {
  refreshToken: refreshToken,
};
