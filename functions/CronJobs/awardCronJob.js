const awardService = require("../routes/services/AwardBadgeService");
const logger = require("firebase-functions").logger;
const getMasterToken = require("./config/getMasterToken");
const FlattenPathwaysMetadata = require("./metadata/flattenPathways/flattenPathways.metadata");
const getUsersInfoScheme = require("./schemes/getUsersInfo.scheme");
const { checkID, checkNodeNeeds } = require("./utils");

const flattenPathwaysMetadata = new FlattenPathwaysMetadata();

const cleanBackpack = (badges) => {
  const result = [];
  for (const badge of badges) {
    if (Object.keys(badge).length !== 1) {
      result.push(badge);
    }
  }
  return result;
};

const awardCronFunction = async (context) => {
  const [masterTokenError, masterTokenResponse] = await getMasterToken();

  if (masterTokenError) {
    logger.error(masterTokenResponse);
    return null;
  }

  const usersInfos = await getUsersInfoScheme();

  const flattenPathways = await flattenPathwaysMetadata.getAllFlattenPathways();

  Object.values(flattenPathways).map((pathway) => pathway.reverse());

  for (let i = 0; i < usersInfos.length; i++) {
    const userPathways = [];
    for (const pathwayID of usersInfos[i].pathways) {
      if (
        typeof flattenPathways[pathwayID.replace(/['"]+/g, "")] !== "undefined"
      ) {
        userPathways.push(flattenPathways[pathwayID.replace(/['"]+/g, "")]);
      }
    }

    const neededBadges = [];
    for (const pathway of userPathways) {
      usersInfos[i].badges.badges = cleanBackpack(usersInfos[i].badges.badges);
      for (const node of pathway) {
        if (node.type !== "requiredBadge") {
          if (!checkID(node.id, usersInfos[i].badges.badges)) {
            if (checkNodeNeeds(node, usersInfos[i].badges.badges)) {
              if (node.type === "completionBadge") {
                neededBadges.push(node.id);
              }
              usersInfos[i].badges.badges.push({ id: node.id });
            }
          }
        }
      }
    }

    logger.info("email: ", usersInfos[i].user.email);
    logger.info("needs: ", neededBadges);
    if (neededBadges.length !== 0) {
      neededBadges.forEach(async (badge) => {
        logger.info("Awarding " + badge + "...");
        let data = {
          email: usersInfos[i].user.email,
          badgeToken: badge,
        };
        await awardService.awardBadge(data, masterTokenResponse);
      });
    }
  }

  return null;
};

module.exports = {
  awardCronFunction: awardCronFunction,
};
