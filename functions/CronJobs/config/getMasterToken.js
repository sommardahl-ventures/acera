const envs = require("../../env.json");
const axios = require("axios");
const logger = require("firebase-functions").logger;

async function getMasterToken() {
  const USER = envs.service.user;
  const PASSWORD = envs.service.password;

  const data = `username=${encodeURIComponent(
    USER
  )}&password=${encodeURIComponent(PASSWORD)}`;

  return await axios({
    headers: {
      "content-type": "application/x-www-form-urlencoded",
    },
    method: "post",
    url: "https://api.badgr.io/o/token",
    data,
  })
    .then((res) => {
      logger.log(res.data.access_token);
      return [false, res.data.access_token];
    })
    .catch((error) => {
      const errorMsg = new Error(`${error.message} - get master token error`);
      logger.error(errorMsg);
      return [true, errorMsg];
    });
}

module.exports = getMasterToken;
