const fireAdmin = require("../../FirebaseU/FirebaseAdmin");
const envs = require("../../env.json");
const APP_URL = envs.service.app_url;
const axios = require("axios");
const {
  flatten,
  buildUserInfo,
  findUser
} = require('../utils')

async function getUsersInfoScheme() {
  const users = await fireAdmin.getUsers()
        .once("value")
        .then((snapshot) => Object.values(snapshot.val()));

  const groups = await fireAdmin.getGroupsAdmin()
        .once("value")
        .then((snapshot) => Object.values(snapshot.val()));

  const usersInfos = [];

  for (let i = 0; i < users.length; i++) {
    let userGroups = [];
    let userPaths = [];
    if (users[i].backpacks) {
      const user_access = Object.values(users[i].backpacks)[0].data
        .access_token;
      const userBackpack = await axios.post(`${APP_URL}api/users/backpack`, {
        token: user_access,
      });
      if (userBackpack.data) {
        userGroups = groups.filter(
          (group) =>
          group.users &&
          group.pathways &&
            findUser(Object.values(group.users), users[i].profile.email)
        );

        const paths = userGroups.map((group) => Object.values(group.pathways));

        userPaths = flatten(paths).map(JSON.stringify);
      }
      const userInfo = buildUserInfo(
        users[i].profile.email,
        user_access,
        userBackpack.data,
        userPaths,
        userGroups
      );
      usersInfos.push(userInfo);
    }
  }
  return usersInfos;
}


module.exports = getUsersInfoScheme
