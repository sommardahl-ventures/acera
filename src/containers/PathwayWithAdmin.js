import React from 'react'
import IsAdminValidation from '../components/Auth/isAdmin'
import Pathway from '../components/Pathway/Pathway'

export const PathwayWithAdmin = ({...props}) => {
    return (
        <IsAdminValidation component={Pathway} {...props} />
    )
}
