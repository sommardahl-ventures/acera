import React, { useState } from "react";
import { Form, Modal } from "react-bootstrap";
import { useInputValue } from "../../hooks/useInputValue";
import Button from "react-bootstrap/Button";
import { editWebhook } from "../../api/Webhooks/editWebhook";

export const EditWebhook = ({ showEditModal, closeEditModal, key, webhookInfo }) => {
  const name = useInputValue(webhookInfo.name);
  const url = useInputValue(webhookInfo.url);
  const secret = useInputValue(webhookInfo.secret);
  const issuer = useInputValue(webhookInfo.issuer);
  const [disabled, setDisable] = useState(false);
  const [error, setError] = useState("");
  const [event, setEvent] = useState(webhookInfo.event);

  const validateFields = () => {
    let error = false;
    if (issuer.value === "") {
      return [(error = true), "The Issuer field it's required"];
    }
    if (event === "") {
      return [(error = true), "The event field it's required"];
    }
    if (url.value === "") {
      return [(error = true), "the URL field it's required"];
    }
    return [error, ""];
  };

  const HandleEditWebhook = async () => {
    const [validationError, errorInfo] = validateFields();
    if (!validationError) {
      const error = await editWebhook({
        key:webhookInfo.key,
        issuer: issuer.value,
        name: name.value,
        url: url.value,
        secret: secret.value,
        event,
      });
      if (error) {
        setError(error);
      } else {
        closeEditModal();
      }
    } else {
      setError(errorInfo);
      setDisable(true);
    }
  };
  return (
    <Modal show={showEditModal} onHide={() => closeEditModal()}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Webhook</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="webhook.issuer">
            <Form.Label>Issuer ID</Form.Label>
            <Form.Control value={issuer.value} onChange={issuer.onChange} />
          </Form.Group>
          <Form.Group controlId="webhook.name">
            <Form.Label>Name</Form.Label>
            <Form.Control value={name.value} onChange={name.onChange} />
          </Form.Group>
          <Form.Group controlId="webhook.url">
            <Form.Label>URL</Form.Label>
            <Form.Control value={url.value} onChange={url.onChange} />
          </Form.Group>
          <Form.Group controlId="webhook.secret">
            <Form.Label>Secret</Form.Label>
            <Form.Control value={secret.value} onChange={secret.onChange} />
          </Form.Group>
          <Form.Group controlId="webhook.event">
            <Form.Label>Event</Form.Label>
            <Form.Control
              as="select"
              value={event}
              onChange={(e) => setEvent(e.target.value)}
            >
              <option>Badge Awarded</option>
              <option>Badge Requested</option>
              <option>Group Invitation</option>
              <option>Pathway Join Requested</option>
              <option>New Pathway</option>
              <option>New Pathway Member</option>
            </Form.Control>
          </Form.Group>
        </Form>
        {error && (
          <div class="alert alert-danger" role="alert">
            {error}
          </div>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => closeEditModal()}>
          Close
        </Button>
        <Button
          variant="primary"
          disabled={disabled}
          onClick={HandleEditWebhook}
        >
          Add
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
