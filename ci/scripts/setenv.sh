#! /bin/bash

function set_staging_vars()
{
    if [ "${CI_COMMIT_REF_NAME}" = "staging" ]; then
        export REACT_APP_RETURN_URL=${REACT_APP_RETURN_URL_STAGING}
        export APP_URL=${APP_URL_STAGING}
        export REACT_APP_FB_DATABASE=${REACT_APP_FB_DATABASE_STAGING}
    fi
}

function set_prod_vars()
{
    if [ "${CI_COMMIT_REF_NAME}" = "master" ]; then
        export REACT_APP_RETURN_URL=${REACT_APP_RETURN_URL_PROD}
        export APP_URL=${APP_URL_PROD}
        export REACT_APP_FB_DATABASE=${REACT_APP_FB_DATABASE_PROD}
    fi
}

# CALLING FUNCTIONS
set_prod_vars
set_staging_vars